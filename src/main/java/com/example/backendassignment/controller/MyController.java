package com.example.backendassignment.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyController {

    @GetMapping("/")
    public String welcome() {
        return "Hello and welcome to the backend assignment!!";
    }

}
