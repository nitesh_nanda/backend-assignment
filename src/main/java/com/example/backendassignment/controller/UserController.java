package com.example.backendassignment.controller;

import com.example.backendassignment.entity.User;
import com.example.backendassignment.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/add-user/{name}/{email}")
    public ResponseEntity<User> addUser(@PathVariable(value = "name") String name, @PathVariable("email") String email){
        return new ResponseEntity<>(userService.addUser(name,email), HttpStatus.OK);
    }

    @GetMapping("/get-user")
    public ResponseEntity<User> getUser(@RequestParam(value = "name") String name){
        return new ResponseEntity<>(userService.getUser(name), HttpStatus.OK);
    }
}
