package com.example.backendassignment.service;

import com.example.backendassignment.entity.User;
import com.example.backendassignment.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User addUser(String name, String email) {
        User user = new User();
        user.setName(name);
        user.setEmail(email);
        return this.userRepository.save(user);
    }

    public User getUser(String name) {
        return this.userRepository.findByName(name);
    }
}
